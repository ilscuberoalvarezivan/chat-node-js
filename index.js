const debug = require('debug')('http')
const pug = require('pug');
const express = require('express');
const app = express();
const router = express.Router();
const port = 8000;

var server = require("http")
    .createServer(app)
    .listen(port, () => {
        console.log(`Example app listening on port ${port}!`);
        debug("listening");
    });

app.use(express.static(__dirname + "/views"));

router.get("/", function (req, res, next) {
    res.render("index.pug", { title: "Express" });
});

router.get("/incidencia", function (req, res, next) {
    res.render("incidencia.pug", { title: "Express" });
});

router.get("/listaIncidencia", function (req, res, next) {
    res.render("listaIncidencia.pug", { title: "Express" });
});

app.use("/", router);

module.exports = app;